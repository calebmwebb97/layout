package com.example.layout

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onArmsClick(view: View)
    {
        if(ArmsCheck.isChecked){
            Arms.visibility = View.VISIBLE
        }
        else{
            Arms.visibility = View.INVISIBLE
        }
    }

    fun onEarsClick(view: View)
    {
        if(EarsCheck.isChecked){
            Ears.visibility = View.VISIBLE
        }
        else{
            Ears.visibility = View.INVISIBLE
        }
    }

    fun onEyebrowsClick(view: View)
    {
        if(EyebrowsCheck.isChecked){
            Eyebrows.visibility = View.VISIBLE
        }
        else{
            Eyebrows.visibility = View.INVISIBLE
        }
    }

    fun onEyesClick(view: View)
    {
        if(EyesCheck.isChecked){
            Eyes.visibility = View.VISIBLE
        }
        else{
            Eyes.visibility = View.INVISIBLE
        }
    }

    fun onGlassesClick(view: View)
    {
        if(GlassesCheck.isChecked){
            Glasses.visibility = View.VISIBLE
        }
        else{
            Glasses.visibility = View.INVISIBLE
        }
    }

    fun onHatClick(view: View)
    {
        if(HatCheck.isChecked){
            Hat.visibility = View.VISIBLE
        }
        else{
            Hat.visibility = View.INVISIBLE
        }
    }

    fun onMouthClick(view: View)
    {
        if(MouthCheck.isChecked){
            Mouth.visibility = View.VISIBLE
        }
        else{
            Mouth.visibility = View.INVISIBLE
        }
    }

    fun onMustacheClick(view: View)
    {
        if(MustacheCheck.isChecked){
            Mustache.visibility = View.VISIBLE
        }
        else{
            Mustache.visibility = View.INVISIBLE
        }
    }

    fun onNoseClick(view: View)
    {
        if(NoseCheck.isChecked){
            Nose.visibility = View.VISIBLE
        }
        else{
            Nose.visibility = View.INVISIBLE
        }
    }

    fun onShoesClick(view: View)
    {
        if(ShoesCheck.isChecked){
            Shoes.visibility = View.VISIBLE
        }
        else{
            Shoes.visibility = View.INVISIBLE
        }
    }
}
